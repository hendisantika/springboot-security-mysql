CREATE TABLE user
(
    id       BIGINT(20) NOT NULL,
    ENABLED  BIT(1)       DEFAULT NULL,
    PASSWORD VARCHAR(125) DEFAULT NULL,
    username VARCHAR(25)  DEFAULT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE authority
(
    id        BIGINT (20) NOT NULL,
    authority VARCHAR(255) DEFAULT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE authorities_users
(
    user_id      BIGINT (20) NOT NULL,
    authority_id BIGINT (20) NOT NULL,
    PRIMARY KEY (user_id, authority_id),
    KEY          FK1hk335nyb5icwqy64y2mhov2v(authority_id)
);

ALTER TABLE authorities_users
    ADD CONSTRAINT FK1hk335nyb5icwqy64y2mhov2v
        FOREIGN KEY (authority_id)
            REFERENCES authority (id);

ALTER TABLE authorities_users
    ADD CONSTRAINT FKc4eanbpu2l39uxbju4u2fm2f1
        FOREIGN KEY (user_id)
            REFERENCES user (id);

CREATE TABLE hibernate_sequence
(
    next_val BIGINT(20) DEFAULT NULL
);