package com.hendisantika.springbootsecuritymysql.service;

import com.hendisantika.springbootsecuritymysql.entity.Authority;
import com.hendisantika.springbootsecuritymysql.entity.User;
import com.hendisantika.springbootsecuritymysql.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-security-mysql
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 22/03/20
 * Time: 07.21
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        //Search for the user with the repository and if it does not exist throw an exception
        User appUser =
                userRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException("User " + username + " does Not exist!"));

        //Map our Authority list to that of spring security
        List grantList = new ArrayList();
        for (Authority authority : appUser.getAuthority()) {
            // ROLE_USER, ROLE_ADMIN,..
            GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(authority.getAuthority());
            grantList.add(grantedAuthority);
        }

        //Create the UserDetails object to be logged and return it.
        UserDetails user = new org.springframework.security.core.userdetails.User(appUser.getUsername(),
                appUser.getPassword(), grantList);
        return user;
    }
}
